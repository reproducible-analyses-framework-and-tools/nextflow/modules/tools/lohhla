process convert_alleles_to_lohhla_format {

  label 'lohhla_container'
  label 'lohhla'

  input:
  tuple val(pat_name), val(run), val(dataset), path(alleles)

  output:
  tuple val(pat_name), val(run), val(dataset), path("*.lohhla.alleles"), emit: lohhla_alleles

  script:
  """
  cat ${alleles} |\
  sed 's/HLA-A/hla_a_/g' |\
  sed 's/HLA-B/hla_b_/g' |\
  sed 's/HLA-C/hla_c_/g' |\
  sed 's/:/_/g' |\
  sed "s/'//g" |\
  sed -z 's/,/\\\n/g' > ${dataset}-${pat_name}-${run}.lohhla.alleles
  """
}

process lohhla {

  label 'lohhla_container'
  label 'lohhla'

  input:
  tuple val(pat_name), val(dataset), val(norm_run), path(norm_bam), path(norm_bai), val(tumor_run), path(tumor_bam), path(tumor_bai), path(alleles)
  path ref

  script:
  """
  Rscript /usr/LOHHLA.R \
  --patientId ${dataset}-${pat_name}-${norm_run}_${tumor_run} \
  --outputDir .  \
  --normalBAMfile ${norm_bam} \
  --BAMDir . \
  --hlaPath ${alleles} \
  --HLAfastaLoc ${ref} \
  --CopyNumLoc FALSE
  """
}
